// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory.alt.spring;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.refcodes.data.Encoding;
import org.refcodes.factory.BeanFactory;
import org.refcodes.factory.InstantiationRuntimeException;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

/**
 * This factory looks up beans from a spring context as provided in the
 * constructor.
 */
public class SpringBeanFactory implements BeanFactory<String> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private URI[] _confFileURIs = null;

	private URI[] _propertyFileURIs = null;

	private Map<String, String> _properties = null;

	private FileSystemXmlApplicationContext _applicationContext;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a factory with the given Spring configuration files.
	 * 
	 * @param aConfigurationFiles The URIs of the configuration files describing
	 *        the handled Spring application context.
	 * 
	 * @throws MalformedURLException If a given URI is malformed.
	 */
	public SpringBeanFactory( URI[] aConfigurationFiles ) throws MalformedURLException {
		doInitialize( aConfigurationFiles, null, null );
	}

	/**
	 * Creates a factory with the given Spring configuration files. If the
	 * configuration files contains placeholders (e.g. ${jdbc.url}), the given
	 * property files are used to replace them. The provided property files have
	 * to follow the JAVA properties scheme (key=value).
	 * 
	 * @param aConfigurationFiles The URIs of the configuration files describing
	 *        the handled Spring application context.
	 * @param aPropertyFiles The URIs of the property files containing the
	 *        values for the used placeholders.
	 * 
	 * @throws MalformedURLException If a given URI is malformed
	 */
	public SpringBeanFactory( URI[] aConfigurationFiles, URI[] aPropertyFiles ) throws MalformedURLException {
		doInitialize( aConfigurationFiles, aPropertyFiles, null );
	}

	/**
	 * Creates a factory with the given Spring configuration files and the given
	 * properties. If the configuration files contains placeholders (e.g.
	 * ${jdbc.url}), the given properties are used to replace them.
	 * 
	 * @param aConfigurationFiles The URIs of the configuration files describing
	 *        the handled Spring application context.
	 * @param aProperties The dynamic properties which are not defined by an
	 *        configuration file.
	 * 
	 * @throws MalformedURLException If a given URI is malformed
	 */
	public SpringBeanFactory( URI[] aConfigurationFiles, Map<String, String> aProperties ) throws MalformedURLException {
		doInitialize( aConfigurationFiles, null, aProperties );
	}

	/**
	 * Creates a factory with the given Spring configuration files and the given
	 * properties. If the configuration files contains placeholders (e.g.
	 * ${jdbc.url}), the given property files and properties are used to replace
	 * them. The provided property files have to follow the JAVA properties
	 * scheme (key=value).
	 * 
	 * @param aConfigurationFiles The URIs of the configuration files describing
	 *        the handled Spring application context.
	 * @param aPropertyFiles The URIs of the property files containing the
	 *        values for the used placeholders.
	 * @param aProperties The dynamic properties which are not defined by an
	 *        configuration file.
	 * 
	 * @throws MalformedURLException If a given URI is malformed
	 */
	public SpringBeanFactory( URI[] aConfigurationFiles, URI[] aPropertyFiles, Map<String, String> aProperties ) throws MalformedURLException {
		doInitialize( aConfigurationFiles, aPropertyFiles, aProperties );
		_confFileURIs = aConfigurationFiles;
		_propertyFileURIs = aPropertyFiles;
		_properties = aProperties;
	}

	/**
	 * Initialize the application context of this factory instance. If the
	 * configuration files contains placeholders (e.g. ${jdbc.url}), you have to
	 * provide property files which contains the used placeholders. This method
	 * will automatically replace them if found. If a placeholder is used in the
	 * configuration files but isn't defined in the property files, the affected
	 * beans arn't created.
	 *
	 * @param aConfigurationFiles The URIs of the configuration files to build
	 *        the Spring application context.
	 * @param aPropertyFiles The URIs of the property files to replace the
	 *        existing placeholders in the given configuration files.
	 * @param aProperties the properties
	 * 
	 * @throws MalformedURLException if a given URI is malformed
	 */
	private void doInitialize( URI[] aConfigurationFiles, URI[] aPropertyFiles, Map<String, String> aProperties ) throws MalformedURLException {

		_confFileURIs = aConfigurationFiles;
		_propertyFileURIs = aPropertyFiles;
		_properties = aProperties;

		// Create a String array to use for the FileSystemXmlApplicationContext
		final String[] configLocations = new String[aConfigurationFiles.length];
		for ( int i = 0; i < aConfigurationFiles.length; i++ ) {
			try {
				configLocations[i] = URLDecoder.decode( aConfigurationFiles[i].toString(), Encoding.UTF_8.getCode() );
			}
			catch ( UnsupportedEncodingException e ) {
				throw new MalformedURLException( "Unsupported encoding in URI \"" + aConfigurationFiles[i].toString() + "\": " + e.getMessage() );
			}
		}
		// Create the application context
		_applicationContext = new FileSystemXmlApplicationContext();
		_applicationContext.setValidating( false );

		PropertySourcesPlaceholderConfigurer placeholderConfigurer = null;

		// If a properties object was given, add it to the
		// PropertyPlaceholderConfigurer
		if ( aProperties != null ) {
			if ( placeholderConfigurer == null ) {
				placeholderConfigurer = new PropertySourcesPlaceholderConfigurer();
			}
			placeholderConfigurer.setProperties( toProperties( aProperties ) );
		}

		// If there are property files given create a Resource array containing
		// them
		if ( aPropertyFiles != null ) {
			final Resource[] propertyLocations = new Resource[aPropertyFiles.length];
			for ( int i = 0; i < aPropertyFiles.length; i++ ) {
				try {
					propertyLocations[i] = new UrlResource( URLDecoder.decode( aPropertyFiles[i].toString(), Encoding.UTF_8.getCode() ) );
				}
				catch ( UnsupportedEncodingException e ) {
					throw new MalformedURLException( "Unsupported encoding in URI \"" + aPropertyFiles[i].toString() + "\": " + e.getMessage() );
				}
			}

			// Create PropertyPlaceholderConfigurer to replace all placeholders
			// inside of the given configuration files
			if ( placeholderConfigurer == null ) {
				placeholderConfigurer = new PropertySourcesPlaceholderConfigurer();
			}
			placeholderConfigurer.setLocations( propertyLocations );

		}

		if ( placeholderConfigurer != null ) {
			// Add the previously created PropertyPlaceholderConfigurer as post
			// processor to the application context
			_applicationContext.addBeanFactoryPostProcessor( placeholderConfigurer );
		}

		// Refresh the application context. This is needed to let the post
		// processors do their work
		_applicationContext.setConfigLocations( configLocations );
		_applicationContext.refresh();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To instance.
	 *
	 * @param <T> the generic type
	 * @param aIdentifier the identifier
	 * 
	 * @return the t
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T create( String aIdentifier ) {
		return (T) _applicationContext.getBean( aIdentifier );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T create( String aIdentifier, Map<String, String> aProperties ) {
		try {
			final Map<String, String> theProperties = new HashMap<>();
			if ( _properties != null ) {
				theProperties.putAll( _properties );
			}
			theProperties.putAll( aProperties );
			final SpringBeanFactory theSpringFactory = new SpringBeanFactory( _confFileURIs, _propertyFileURIs, theProperties );
			final T instance = theSpringFactory.create( aIdentifier );
			return instance;
		}
		catch ( MalformedURLException e ) {
			throw new InstantiationRuntimeException( e );
		}
	}

	/**
	 * To instances.
	 *
	 * @param <T> the generic type
	 * @param aType the type
	 * 
	 * @return the sets the
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> Set<T> create( Class<?> aType ) {
		final Map<String, T> theBeansOfType = (Map<String, T>) _applicationContext.getBeansOfType( aType, true, true );
		return new HashSet<>( theBeansOfType.values() );
	}

	/**
	 * To instances.
	 *
	 * @param <T> the generic type
	 * @param aType the type
	 * @param aProperties the properties
	 * 
	 * @return the sets the
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> Set<T> toInstances( Class<?> aType, Map<String, String> aProperties ) {
		try {
			final Map<String, String> theProperties = new HashMap<>();
			if ( _properties != null ) {
				theProperties.putAll( _properties );
			}
			theProperties.putAll( aProperties );
			final SpringBeanFactory theSpringFactory = new SpringBeanFactory( _confFileURIs, _propertyFileURIs, theProperties );
			final Map<String, T> theBeansOfType = (Map<String, T>) theSpringFactory._applicationContext.getBeansOfType( aType, true, true );
			return new HashSet<>( theBeansOfType.values() );
		}
		catch ( MalformedURLException e ) {
			throw new InstantiationRuntimeException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private Properties toProperties( Map<String, String> aProperties ) {
		final Properties theProperties = new Properties();
		theProperties.putAll( aProperties );
		return theProperties;
	}
}