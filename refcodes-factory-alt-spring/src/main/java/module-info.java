module org.refcodes.factory.alt.spring {
	requires org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires spring.beans;
	requires spring.context;
	requires spring.core;
	requires transitive org.refcodes.factory;

	exports org.refcodes.factory.alt.spring;
}
