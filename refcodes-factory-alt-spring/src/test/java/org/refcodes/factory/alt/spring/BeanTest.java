// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.factory.alt.spring;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.net.URI;
import org.junit.jupiter.api.Test;
import org.refcodes.factory.BeanLookupFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Tests the spring bean.
 */
public class BeanTest {

	private static final String TEST_BEAN = "testBean";

	/**
	 * Test test bean.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTestBean() throws IOException {
		final Resource theFactoryContextResource = new ClassPathResource( "refcodes-factory-context.xml" );
		final Resource theFactoryConfigResource = new ClassPathResource( "refcodes-factory.conf" );
		final BeanLookupFactory<String> theLookupFactory = new SpringBeanFactory( new URI[] { theFactoryContextResource.getURI() }, new URI[] { theFactoryConfigResource.getURI() } );
		final TestBean theTestBean = theLookupFactory.create( TEST_BEAN );
		assertEquals( theTestBean.getFirstName(), "First" );
		assertEquals( theTestBean.getLastName(), "Last" );
		assertEquals( theTestBean.getPassword(), "Hallo Welt" );
	}
}
